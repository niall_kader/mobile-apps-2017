package com.wtcweb.mobileapps2017.niall;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.wtcweb.mobileapps2017.R;

public class ReceiveNotificationActivity extends AppCompatActivity {

    public static final String NOTIFICATION_EXTRA = "notificationExtra";

    TextView txtReceiveNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_notification);

        txtReceiveNotification = (TextView)findViewById(R.id.lblReceiveNotification);
        txtReceiveNotification.setText("This Activity was created to handle intents that were stuffed into a Notification that was created in NotificationActivity. The toast that pops up here as 'extra' data that was put into the Intent.");

        Intent intent = getIntent();
        String toastMessage = intent.getStringExtra(NOTIFICATION_EXTRA);
        if(toastMessage == null){
            toastMessage = "This activity was NOT launched by a Notification in NotificationActivity";
        }

        Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show();// don't forget to show()!!!!!
    }
}
